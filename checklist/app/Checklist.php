<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{

    protected $primaryKey = 'checklist_id';
    protected $table = 'checklist';
    protected $guarded = [];

    public function items()
    {
        return $this->hasMany('App\Item', 'checklist_id');
    }
}
