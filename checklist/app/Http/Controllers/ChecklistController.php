<?php

namespace App\Http\Controllers;

use DateTime;
use App\Checklist;
use App\Item;
use App\User;
use Illuminate\Http\Request;

class ChecklistController extends Controller
{   

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $req) {
        
        $page_limit = $req->has('page_limit') && $req->input('page_limit') != null ? $req->input('page_limit') : 10;
        $page_offset = $req->has('page_offset') && $req->input('page_offset') != null  ? $req->input('page_offset') : 0;
        $checklist_index = route('checklist.index');
        // route('checklist.index') . '?page_limit=' . $page_limit .'&'. 'page_offset' . $page_offset

        if($req->has('include') && ($req->input('include') == 'items')) {
            $checklist_data = Checklist::with('items');
        } else {
            $checklist_data = new Checklist;
        }

        
        // object_domain
        // object_id
        // description
        // is_completed
        // completed_at
        $key_column =  array_keys($req->input('filter'))[0];
        // return $req->input('filter[:attribute_name][like]=:value');
        
        if($req->has('filter')) {
            $filter = $req->input('filter');

            if(isset($filter[$key_column]['like'])) {
                $checklist_data = $checklist_data->where($key_column, 'like', $filter[$key_column]['like']);
            }
            
            if(isset($filter[$key_column]['!like'])) {
                $checklist_data = $checklist_data->where($key_column, 'not like', $filter[$key_column]['!like']);
            }
            
            if(isset($filter[$key_column]['is'])) {
                $checklist_data = $checklist_data->where($key_column, '=', $filter[$key_column]['is']);
            }
            
            if(isset($filter[$key_column]['!is'])) {
                $checklist_data = $checklist_data->where($key_column, '!=', $filter[$key_column]['!is']);
            }

            if(isset($filter[$key_column]['in'])) {
                $checklist_data = $checklist_data->whereIn($key_column, explode(',', $filter[$key_column]['in']));
            }
            if(isset($filter[$key_column]['!in'])) {
                $checklist_data = $checklist_data->whereNotIn($key_column, explode(',', $filter[$key_column]['!in']));
            }
        }

        if($req->has('sort')) {
            $sort_type = (substr($req->input('sort'), 0, 1) == '-') ? 'DESC' : 'ASC';
            $column = ltrim($req->input('sort'), '-');
            $checklist_data = $checklist_data->orderBy($column, $sort_type);
        }


        $checklist_data = $checklist_data->skip($page_offset)->take($page_limit)->get();

        $count = $page_limit;
        $total = Checklist::count();
        $last_page = (int) $total / ((int) $count);


        $response_adapter = [
            'meta' => [
                'count' => $count,
                'total' => $total
            ],
            'links' => [
                'first' => $checklist_index . '?page_limit=' . $page_limit .'&'. 'page_offset=' . $page_offset,
                'last' => round($last_page) == $page_offset ? 'null' : $checklist_index . '?page_limit=' . $page_limit .'&'. 'page_offset=' . round($last_page),
                'next' => $checklist_index . '?page_limit=' . $page_limit .'&'. 'page_offset=' . ($page_offset + 1),
                'prev' => $page_offset == 0 ? 'null' : $checklist_index . '?page_limit=' . $page_limit .'&'. 'page_offset=' . ($page_offset - 1)
                // 'first' => $checklist_index . '?page[limit]=' . $page_limit .'&'. 'page[offset]=' . $page_offset,
                // 'last' => round($last_page) == $page_offset ? 'null' : $checklist_index . '?page[limit]=' . $page_limit .'&'. 'page[offset]=' . round($last_page),
                // 'next' => $checklist_index . '?page[limit]=' . $page_limit .'&'. 'page[offset]=' . ($page_offset + 1),
                // 'prev' => $page_offset == 0 ? 'null' : $checklist_index . '?page[limit]=' . $page_limit .'&'. 'page[offset]=' . ($page_offset - 1)
            ],
            'data'=> []
        ];

        foreach($checklist_data as $checklist) {

            $checklist_adapter = [
                  'type'=> 'checklists',
                  'id'=> $checklist->checklist_id,
                  'attributes'=> [
                    'object_domain'=> $checklist->object_domain,
                    'object_id'=> $checklist->object_id,
                    'description'=> $checklist->description,
                    'is_completed'=> (boolean) $checklist->is_completed,
                    'due'=> null,
                    'urgency'=> $checklist->urgency,
                    'completed_at'=> null,
                    'last_update_by'=> null,
                    'updated_at'=> null,
                    'created_at'=> gmdate(DATE_ATOM, strtotime($checklist->created_at->toDateTimeString()))
                ],
                  'links'=> [
                    'self'=> route('checklist.show', ['checklistId' => $checklist->checklist_id])
                    ]
            ];

            if($req->has('include') && ($req->input('include') == 'items')) { 
                $checklist_adapter['data']['attributes']['items'] = $checklist->items;
            }

            array_push($response_adapter['data'], $checklist_adapter);
        }

        return $response_adapter;
    }

    public function show(Request $req, $checklistId) {
        try {


            $checklist = Checklist::find($checklistId);
            $params = $req->input('');

            if(empty($checklist)) {
                return response(json_encode($error_msg), 404);
            }

            $success_msg = [
                'data'=> [
                  'type'=> 'checklists',
                  'id'=> $checklist->checklist_id,
                  'attributes'=> [
                    'object_domain'=> $checklist->object_domain,
                    'object_id'=> $checklist->object_id,
                    'description'=> $checklist->description,
                    'is_completed'=> (boolean) $checklist->is_completed,
                    'due'=> null,
                    'urgency'=> $checklist->urgency,
                    'completed_at'=> null,
                    'last_update_by'=> null,
                    'updated_at'=> null,
                    'created_at'=> gmdate(DATE_ATOM, strtotime($checklist->created_at->toDateTimeString()))
                ],
                  'links'=> [
                    'self'=> route('checklist.show', ['id' => $checklist->checklist_id])
                    ]
                ]
            ];

            if(($req->has('include') && $req->input('include') == 'items')) {
                $success_msg['data']['attributes']['items'] = 
                    Item::where('checklist_id', $checklist->checklist_id)
                    ->get()->toArray();

                for($i = 0; $i < count($success_msg['data']['attributes']['items']); $i++) {
                    $success_msg['data']['attributes']['items'][$i]['created_at'] = gmdate(DATE_ATOM, strtotime($success_msg['data']['attributes']['items'][$i]['created_at']));
                    $success_msg['data']['attributes']['items'][$i]['updated_at'] = gmdate(DATE_ATOM, strtotime($success_msg['data']['attributes']['items'][$i]['updated_at']));
                }
            }

            return response(json_encode($success_msg), 201)->header('Content-Type', 'application/json');

        } catch(Exception $e) {
            $error_msg = [
                'status'=>'500',
                'error'=> 'Server Error'
            ];
            return response(json_encode($error_msg), 500);
        }
    }

    public function store(Request $req) 
    {
        //gmdate('d.m.Y H:i', strtotime(new DateTime()))
        // 'due' => gmdate('d.m.Y H:i', strtotime(($r['due']))),

        try {
            $r = (array) json_decode($req->getContent())->data->attributes;

            $checklist = Checklist::create([
                'object_domain' => $r['object_domain'],
                'object_id' => $r['object_id'],
                'user_id' => User::getUserIDByToken($req),
                'description' => $r['description'],
                // 'is_completed' => $r['is_completed'],
                'completed_at' => null,
                'updated_by' => User::getUserIDByToken($req),
                'due' => date('Y-m-d H:i:s', strtotime($r['due'])),
                'urgency' => $r['urgency'],
            ]);

            foreach($r['items'] as $i) {
                $item = Item::create([
                    'checklist_id' => $checklist->checklist_id,
                    'description' => $i
                ]);
            }

            $success_msg = [
                'data'=> [
                  'type'=> 'checklists',
                  'id'=> $checklist->checklist_id,
                  'attributes'=> [
                    'object_domain'=> $checklist->object_domain,
                    'object_id'=> $checklist->object_id,
                    'description'=> $checklist->description,
                    'is_completed'=> (boolean) $checklist->is_completed,
                    'due'=> null,
                    'urgency'=> null,
                    'completed_at'=> null,
                    'updated_by'=> null,
                    'updated_at'=> null,
                    'created_at'=> gmdate(DATE_ATOM, strtotime($checklist->created_at->toDateTimeString()))
                ],
                  'links'=> [
                    'self'=> route('checklist.show', ['id' => $checklist->checklist_id])
                    ]
                ]
            ];

            return response(json_encode($success_msg), 201)->header('Content-Type', 'application/json');

        } catch(Exception $e) {
            $error_msg = [
                'status'=>'500',
                'error'=> 'Server Error'
            ];
            return response(json_encode($error_msg), 500);
        }
    }

    public function update(Request $req, $checklistId) {

        try {

            $checklist = Checklist::find($checklistId);
            $error_msg = [
                'status'=>'404',
                'error'=> 'Not Found'
            ];
    
            if(empty($checklist)) {
                return response(json_encode($error_msg), 404);
            }
            $r = (array) json_decode($req->getContent())->data->attributes;

            $checklist->object_domain = $r['object_domain'];
            $checklist->object_id = $r['object_id'];
            $checklist->description = $r['description'];
            $checklist->is_completed = $r['is_completed'];
            $checklist->updated_at =  date('Y-m-d H:i:s', strtotime($r['created_at']));
            $checklist->updated_by =  User::getUserIDByToken($req);
            $checklist->save();

            $success_msg = [
                'data'=> [
                  'type'=> 'checklists',
                  'id'=> $checklist->checklist_id,
                  'attributes'=> [
                    'object_domain'=> $checklist->object_domain,
                    'object_id'=> $checklist->object_id,
                    'description'=> $checklist->description,
                    'is_completed'=> (boolean) $checklist->is_completed,
                    'due'=> null,
                    'urgency'=> $checklist->urgency,
                    'completed_at'=> null,
                    'updated_by'=> null,
                    'updated_at'=> null,
                    'created_at'=> gmdate(DATE_ATOM, strtotime($checklist->created_at->toDateTimeString()))
                ],
                  'links'=> [
                    'self'=> route('checklist.show', ['id' => $checklist->checklist_id])
                    ]
                ]
            ];

            return response(json_encode($success_msg), 201)->header('Content-Type', 'application/json');

        } catch(Exception $e) {
            $error_msg = [
                'status'=>'500',
                'error'=> 'Server Error'
            ];
            return response(json_encode($error_msg), 500);
        }
    }

    public function destroy(Request $req, $checklistId) {
        try {

            $checklist = Checklist::find($checklistId);
            $error_msg = [
                'status'=>'404',
                'error'=> 'Not Found'
            ];
    
            if(empty($checklist)) {
                return response(json_encode($error_msg), 404);
            }

            if(count($checklist->items()) > 0) {
                $error_msg = [
                    'status'=>'500',
                    'error'=> 'Server Error'
                ];
                return response(json_encode($error_msg), 500);
            }

            $checklist->delete();

            return response(204);

        } catch(Exception $e) {
            $error_msg = [
                'status'=>'500',
                'error'=> 'Server Error'
            ];
            return response(json_encode($error_msg), 500);
        }
    }
}
