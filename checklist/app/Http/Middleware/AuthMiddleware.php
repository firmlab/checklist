<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $error_msg = [
            "status"=> "401",
            "error"=> "Not Authorized"
        ];


        if ($request->header('Authorization')) {
            $key = $request->header('Authorization');
            $user = User::where('api_token', $key)->first();

            if(empty($user)){
                return response(json_encode($error_msg), 401);
                // dd($request);
                // $request->request->set('user_auth_id', $user->user_id);
                // $request->merge(['user_auth_id' => $user->user_id]);
                // $request->getBody()->add(['user_auth_id' => $user->user_id]);
                // $req['user_auth_id'] = $user->user_id;
            } 
            // else {
            //     return response(json_encode($error_msg), 401);
            // }
        } else {
            
            return response(json_encode($error_msg), 401);
        }
        // $user = 
        return $next($request);
    }
}
