<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

    protected $table = 'item';
    protected $primaryKey = 'item_id';

    protected $guarded = [];

    public function checklist()
    {
        return $this->belongsTo('App\Checklist');
    }
}
