<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// $router->get('user/{id}', function ($id) {
//     return 'User '.$id;
// });
$router->group(['prefix'=>'api/v1'], function() use($router){ 
    $router->get('checklist', [
        'as' => 'checklist.index', 'uses' => 'ChecklistController@index'
    ]);
});


$router->post('checklist', [
    'middleware' => ['auth'],
    'as' => 'checklist.store', 'uses' => 'ChecklistController@store'
]);

$router->get('checklist/{checklistId}', [
    'middleware' => ['auth'],
    'as' => 'checklist.show', 'uses' => 'ChecklistController@show'
]);

$router->put('checklist/{checklistId}', [
    'middleware' => ['auth'],
    'as' => 'checklist.update', 'uses' => 'ChecklistController@update'
]);

$router->delete('checklist/{checklistId}', [
    'middleware' => ['auth'],
    'as' => 'checklist.destroy', 'uses' => 'ChecklistController@destroy'
]);