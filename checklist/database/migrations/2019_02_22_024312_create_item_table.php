<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item', function (Blueprint $table) {
            $table->increments('item_id');
            $table->unsignedInteger('checklist_id');
            $table->string('description'); //item_name
            $table->boolean('is_completed');
            $table->dateTime('completed_at')->nullable();
            $table->string('updated_by')->nullable();
            $table->dateTime('due')->nullable();
            $table->integer('urgency')->nullable();
            $table->timestamps();

            $table->foreign('checklist_id')
                ->references('checklist_id')->on('checklist')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item');
    }
}
