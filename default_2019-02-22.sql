# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.24)
# Database: default
# Generation Time: 2019-02-22 13:20:48 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table checklist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `checklist`;

CREATE TABLE `checklist` (
  `object_domain` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `object_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `checklist_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `due` datetime DEFAULT NULL,
  `urgency` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`checklist_id`),
  KEY `checklist_user_id_foreign` (`user_id`),
  CONSTRAINT `checklist_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `checklist` WRITE;
/*!40000 ALTER TABLE `checklist` DISABLE KEYS */;

INSERT INTO `checklist` (`object_domain`, `object_id`, `checklist_id`, `user_id`, `description`, `is_completed`, `completed_at`, `updated_by`, `due`, `urgency`, `created_at`, `updated_at`)
VALUES
	('contact','1',1,1,'Need to verify this guy hehehe edited by adit2.',1,NULL,'2','2019-01-25 07:50:14',1,'2019-02-22 06:33:53','2018-01-25 07:50:14'),
	('contact','3',3,1,'Need to verify who are you man.',0,NULL,'1','2019-01-25 07:50:14',2,'2019-02-22 06:52:51','2019-02-22 06:52:51'),
	('contact','4',4,1,'Need to verify who are you man yuyu.',0,NULL,'1','2019-01-25 07:50:14',3,'2019-02-22 07:51:20','2019-02-22 07:51:20');

/*!40000 ALTER TABLE `checklist` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `item`;

CREATE TABLE `item` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `checklist_id` int(10) unsigned NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `due` datetime DEFAULT NULL,
  `urgency` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `item_checklist_id_foreign` (`checklist_id`),
  CONSTRAINT `item_checklist_id_foreign` FOREIGN KEY (`checklist_id`) REFERENCES `checklist` (`checklist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;

INSERT INTO `item` (`item_id`, `checklist_id`, `description`, `is_completed`, `completed_at`, `updated_by`, `due`, `urgency`, `created_at`, `updated_at`)
VALUES
	(1,3,'Visit his house',0,NULL,NULL,NULL,NULL,'2019-02-22 06:52:52','2019-02-22 06:52:52'),
	(2,3,'Capture a photo',0,NULL,NULL,NULL,NULL,'2019-02-22 06:52:52','2019-02-22 06:52:52'),
	(3,3,'Meet him on the house',0,NULL,NULL,NULL,NULL,'2019-02-22 06:52:52','2019-02-22 06:52:52'),
	(4,4,'Visit his house',0,NULL,NULL,NULL,NULL,'2019-02-22 07:51:20','2019-02-22 07:51:20'),
	(5,4,'Capture a photo',0,NULL,NULL,NULL,NULL,'2019-02-22 07:51:20','2019-02-22 07:51:20'),
	(6,4,'Meet him on the house',0,NULL,NULL,NULL,NULL,'2019-02-22 07:51:20','2019-02-22 07:51:20');

/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(7,'2019_02_22_020544_create_users_table',1),
	(8,'2019_02_22_021103_create_checklist_table',1),
	(9,'2019_02_22_024312_create_item_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_api_token_unique` (`api_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `api_token`, `created_at`, `updated_at`)
VALUES
	(1,'firman','faputra115@gmail.com','$2y$12$G.RuMye0C8tohDLQzyV3t.0rTOMQfHGuAGWAyaQbUjohrDLm6Xs5u','$2y$12$G.RuMye0C8tohDLQzyV3t.0rTOMQfHGuAGWAyaQbUjohrDLm6Xs5u',NULL,NULL),
	(2,'adit','fadetia@gmail.com','$2y$12$G.KLHFkndclaf8tohDLQzyV3t.0rTOMQfHGuAGWAyaQbUjohrDLm6Xs5u','$2y$12$G.KLHFkndclaf8tohDLQzyV3t.0rTOMQfHGuAGWAyaQbUjohrDLm6Xs5u',NULL,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
